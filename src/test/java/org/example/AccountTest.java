package org.example;

import org.example.Account;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AccountTest {

    int bound = 1000000;
    int maxCredit = -1000;

    @Test
    void getBalanceAfterAccountCreatedReturnZero() {
        Account acc = new Account();
        Assertions.assertEquals(0, acc.getBalance());
    }

    @Test
    void isBlockedReturnFalseWhenAccountCreated() {
        Account acc = new Account();
        Assertions.assertFalse(acc.isBlocked());
    }

    @Test
    void isBlockedReturnTrueAfterBlockMethod() {
        Account acc = new Account();
        acc.block();
        Assertions.assertTrue(acc.isBlocked());
    }

    @Test
    void isBlockedReturnFalseAfterBlockAndUnblockMethods() {
        Account acc = new Account();
        acc.block();
        acc.unblock();
        Assertions.assertFalse(acc.isBlocked());
    }

    @Test
    void whenUnblockWasSuccessfulReturnTrue() {
        Account acc = new Account();
        Assertions.assertTrue(acc.unblock());
    }

    @Test
    void withdrawSumMoreThanBalancePlusCreditMaxReturnFalseAndBalanceUnchanged() {
        Account acc = new Account();
        int balance = acc.getBalance();
        Assertions.assertFalse(acc.withdraw(maxCredit + 1));
        Assertions.assertEquals(balance, acc.getBalance());
    }

    @Test
    void depositWhenSumIsMoreThanBoundReturnFalseAndBalanceUnchanged() {
        Account acc = new Account();
        int balance = acc.getBalance();
        Assertions.assertFalse(acc.deposit(bound + 1));
        Assertions.assertEquals(balance, acc.getBalance());
    }

    @Test
    void depositNegativeSumReturnFalseAndBalanceUnchanged() {
        Account acc = new Account();
        int balance = acc.getBalance();
        Assertions.assertFalse(acc.deposit(-1));
        Assertions.assertEquals(balance, acc.getBalance());
    }

    @Test
    void withdrawNegativeSumReturnFalseAndBalanceUnchanged() {
        Account acc = new Account();
        int balance = acc.getBalance();
        Assertions.assertFalse(acc.withdraw(-1));
        Assertions.assertEquals(balance, acc.getBalance());
    }

    @Test
    void depositWhenBalanceWillBeMoreThanBoundReturnFalseAndBalanceUnchanged() {
        Account acc = new Account();
        Assertions.assertTrue(acc.deposit(bound));
        int balance = acc.getBalance();
        Assertions.assertFalse(acc.deposit(1));
        Assertions.assertEquals(balance, acc.getBalance());
    }

    @Test
    void withdrawWhenBalanceWillBeLessThanCreditReturnFalseAndBalanceUnchanged() {
        Account acc = new Account();
        Assertions.assertTrue(acc.withdraw(-maxCredit));
        int balance = acc.getBalance();
        Assertions.assertFalse(acc.withdraw(1));
        Assertions.assertEquals(balance, acc.getBalance());
    }

    @Test
    void withdrawWhenAccountIsBlockedReturnFalseAndBalanceUnchanged() {
        Account acc = new Account();
        acc.block();
        int balance = acc.getBalance();
        Assertions.assertFalse(acc.deposit(1));
        Assertions.assertEquals(balance, acc.getBalance());
    }

    @Test
    void depositWhenAccountIsBlockedReturnFalseAndBalanceUnchanged() {
        Account acc = new Account();
        acc.block();
        int balance = acc.getBalance();
        Assertions.assertFalse(acc.withdraw(1));
        Assertions.assertEquals(balance, acc.getBalance());
    }

    @Test
    void whenAccountUnblockedSetMaxCreditReturnFalseAndMaxCreditUnchanged() {
        Account acc = new Account();
        Assertions.assertFalse(acc.setMaxCredit(100000));
        Assertions.assertEquals(-maxCredit, acc.getMaxCredit());
    }

    @Test
    void whenSetMaxCreditReturnAbsMoreThanBoundFalseAndMaxCredit() {
        Account acc = new Account();
        acc.block();
        Assertions.assertFalse(acc.setMaxCredit(bound + 1));
        Assertions.assertEquals(-maxCredit, acc.getMaxCredit());
    }

    @Test
    void whenSetMaxCreditReturnAbsLessThanMinusBoundFalseAndMaxCredit() {
        Account acc = new Account();
        acc.block();
        Assertions.assertFalse(acc.setMaxCredit(-bound - 1));
        Assertions.assertEquals(-maxCredit, acc.getMaxCredit());
    }

    @Test
    void SetMaxCreditReturnTrueAndChange() {
        Account acc = new Account();
        int limit = -100000;
        acc.block();
        Assertions.assertTrue(acc.setMaxCredit(limit));
        Assertions.assertEquals(limit, acc.getMaxCredit());
    }

    // Тесты после мутационного тестирования

    @Test
    void depositZeroSumReturnTrue() {
        Account acc = new Account();
        Assertions.assertTrue(acc.deposit(0));
    }

    @Test
    void withdrawZeroSumReturnTrue() {
        Account acc = new Account();
        Assertions.assertTrue(acc.withdraw(0));
    }

    @Test
    void withdrawBoundSumPlusCreditReturnTrueWhenAfterDepositBoundSum() {
        Account acc = new Account();
        acc.deposit(bound);
        Assertions.assertTrue(acc.withdraw(bound - maxCredit));
    }


    @Test
    void getBalanceShouldReturnInternalFieldBalance() {
        Account acc = new Account();
        int money = 100;
        acc.deposit(money);
        Assertions.assertEquals(money, acc.getBalance());
    }

    @Test
    void unblockReturnFalseAccountWhenBalanceIsLessThanMaxCredit() {
        Account acc = new Account();
        int maxCred = 0;
        acc.withdraw(maxCred + 1);
        acc.block();
        acc.setMaxCredit(maxCred);
        Assertions.assertFalse(acc.unblock());
    }

    @Test
    void unblockReturnTrueAccountWhenBalanceIsEqualToMaxCredit() {
        Account acc = new Account();
        int maxCred = 0;
        acc.withdraw(maxCred);
        acc.block();
        acc.setMaxCredit(maxCred);
        Assertions.assertTrue(acc.unblock());
    }

    @Test
    void setMaxCreditEqualToBoundReturnTrue() {
        Account acc = new Account();
        acc.block();
        Assertions.assertTrue(acc.setMaxCredit(bound));
    }

    @Test
    void setMaxCreditEqualToMinusBoundReturnTrue() {
        Account acc = new Account();
        acc.block();
        Assertions.assertTrue(acc.setMaxCredit(-bound));
    }
}
